package com.tpi.currentpricetest.service;

import com.tpi.currentpricetest.feign.CurrencyPriceFeignClient;
import com.tpi.currentpricetest.model.dao.CurrencyLanguageExchangeDao;
import com.tpi.currentpricetest.model.response.CurrentPriceBpiDetailResp;
import com.tpi.currentpricetest.model.response.CurrentPriceResp;
import com.tpi.currentpricetest.model.response.ExchangeCurrentPriceDetailResp;
import com.tpi.currentpricetest.model.response.ExchangeCurrentPriceResp;
import com.tpi.currentpricetest.repository.CurrencyLanguageExchangeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class CurrencyService {

    @Autowired
    private CurrencyLanguageExchangeRepository currencyLanguageExchangeRepository;

    private final CurrencyPriceFeignClient currencyPriceFeignClient;

    /**
     * 呼叫coinDesk的API
     * @return CurrentPriceResp api回傳json
     */
    public CurrentPriceResp callCoinDesk() {
        try {
            ResponseEntity<CurrentPriceResp> currentPriceResp = currencyPriceFeignClient.callCoinDesk();
            return currentPriceResp.getBody();
        } catch (Exception e){
            log.info("呼叫CoinDeskAPI錯誤:{}" , e.toString());
            throw e;
        }
    }

    /**
     * 呼叫coinDesk並轉換內容的API
     * @return CurrentPriceResp api回傳json
     */
    public ExchangeCurrentPriceResp exchangeCoinDesk() {
        ResponseEntity<CurrentPriceResp> currentPriceRespEnt;
        try {
            currentPriceRespEnt = currencyPriceFeignClient.callCoinDesk();
        } catch (Exception e) {
            log.info("呼叫CoinDeskAPI錯誤:{}" , e.toString());
            throw e;
        }
        CurrentPriceResp currentPriceResp = currentPriceRespEnt.getBody();
        List<CurrencyLanguageExchangeDao> languageExchange;
        try {
            languageExchange = currencyLanguageExchangeRepository.findAll();
        } catch (Exception e) {
            log.info("DataBase錯誤:{}" , e.toString());
            throw e;
        }
        Map<String, ExchangeCurrentPriceDetailResp> exchangeCurrentPriceDetail = new HashMap<>();
        Map<String, CurrentPriceBpiDetailResp> bpi = Objects.requireNonNull(currentPriceResp).getBpi();
        for(Map.Entry<String, CurrentPriceBpiDetailResp> entry : bpi.entrySet()) {

            ExchangeCurrentPriceDetailResp exchangeCurrentPriceDetailResp = new ExchangeCurrentPriceDetailResp();
            BeanUtils.copyProperties(entry.getValue(), exchangeCurrentPriceDetailResp);

            CurrencyLanguageExchangeDao currencyLanguageExchange = languageExchange.stream().filter(item -> item.getEnCurrency().equalsIgnoreCase(entry.getKey())).findAny().orElse(null);
            if(currencyLanguageExchange != null){
                exchangeCurrentPriceDetailResp.setCurrencyZH(currencyLanguageExchange.getZhCurrency());
            }
            exchangeCurrentPriceDetail.put(entry.getKey(), exchangeCurrentPriceDetailResp);
        }

            String isoUpdateTime = Objects.requireNonNull(currentPriceResp.getTime()).getUpdatedISO();
            DateTimeFormatter isoFormatter = DateTimeFormatter.ISO_DATE_TIME;
            LocalDateTime resISOUpdateTime = LocalDateTime.parse(isoUpdateTime , isoFormatter);
            String resUpdateTime = resISOUpdateTime.toString();

            return ExchangeCurrentPriceResp.builder()
                                           .updateTime(resUpdateTime)
                                           .currentPriceDetail(exchangeCurrentPriceDetail)
                                           .build();
    }
}