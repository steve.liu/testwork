package com.tpi.currentpricetest.service;

import com.tpi.currentpricetest.model.dao.CurrencyLanguageExchangeDao;
import com.tpi.currentpricetest.repository.CurrencyLanguageExchangeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class DBService {

    @Autowired
    private CurrencyLanguageExchangeRepository currencyLanguageExchangeRepository;

    /**
     * 新增資料庫資料
     * @param enCurrency 幣別英文
     * @param zhCurrency 幣別中文
     * @return boolean
     */
    public boolean insertData(String enCurrency , String zhCurrency) {
        try {
            Optional<CurrencyLanguageExchangeDao> optionalCLED;
            if(!enCurrency.isEmpty() && zhCurrency.isEmpty()){
                optionalCLED = currencyLanguageExchangeRepository.findByEnCurrency(enCurrency);
            } else if(enCurrency.isEmpty() && !zhCurrency.isEmpty()){
                optionalCLED = currencyLanguageExchangeRepository.findByZhCurrency(zhCurrency);
            } else {
                log.info("錯誤，輸入資料為空");
                return false;
            }

            if (optionalCLED.isPresent()) {
                log.info("資料已經存在");
                return false;
            } else {
                CurrencyLanguageExchangeDao newData = new CurrencyLanguageExchangeDao();
                newData.setEnCurrency(enCurrency);
                newData.setZhCurrency(zhCurrency);
                currencyLanguageExchangeRepository.save(newData);
                return true;
            }

        } catch (Exception e){
            log.info("新增資料失敗:{}" , e.toString());
            throw e;
        }
    }

    /**
     * 修改資料庫資料
     * @param enCurrency 幣別英文
     * @param zhCurrency 幣別中文
     * @return boolean
     */
    public boolean updateData(String enCurrency , String zhCurrency) {
        try {
            checkRequest(enCurrency , zhCurrency).ifPresent(currency -> {
                currency.setEnCurrency(enCurrency);
                currency.setZhCurrency(zhCurrency);
                currencyLanguageExchangeRepository.save(currency);
            });
            return true;
        } catch (Exception e){
            log.info("更新資料失敗:{}" , e.toString());
            throw e;
        }
    }

    /**
     * 刪除資料庫資料
     * @param enCurrency 幣別英文
     * @param zhCurrency 幣別中文
     * @return boolean
     */
    public boolean deleteData(String enCurrency , String zhCurrency) {
        try {
            checkRequest(enCurrency , zhCurrency).ifPresent(currency -> currencyLanguageExchangeRepository.delete(currency));
            return true;
        } catch (Exception e){
            log.info("刪除資料失敗:{}" , e.toString());
            throw e;
        }
    }

    /**
     * 查詢資料庫資料
     * @param enCurrency 幣別英文
     * @param zhCurrency 幣別中文
     * @return CurrencyLanguageExchangeDao 查詢資料結果
     */
    public CurrencyLanguageExchangeDao selectData(String enCurrency , String zhCurrency) {
        return checkRequest(enCurrency , zhCurrency).orElse(null);
    }

    /**
     * 檢查request資料是否存在並回傳資料庫查詢結果
     * @param enCurrency 幣別英文
     * @param zhCurrency 幣別中文
     * @return String
     */
    private Optional<CurrencyLanguageExchangeDao> checkRequest(String enCurrency , String zhCurrency) {
        try {
        Optional<CurrencyLanguageExchangeDao> res;
        if(!enCurrency.isEmpty() && zhCurrency.isEmpty()){
            res = currencyLanguageExchangeRepository.findByEnCurrency(enCurrency);
        } else if(enCurrency.isEmpty() && !zhCurrency.isEmpty()){
            res = currencyLanguageExchangeRepository.findByZhCurrency(zhCurrency);
        } else {
            log.info("錯誤，輸入資料為空");
            return Optional.empty();
        }
        return res;
        } catch (Exception e){
            log.info("查無資料:{}" , e.toString());
            throw e;
        }
    }

}
