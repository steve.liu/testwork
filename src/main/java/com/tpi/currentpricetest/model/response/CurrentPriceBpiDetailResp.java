package com.tpi.currentpricetest.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class CurrentPriceBpiDetailResp {

    private String code;
    private String symbol;
    private String rate;
    private String description;
    private Float rateFloat;

}
