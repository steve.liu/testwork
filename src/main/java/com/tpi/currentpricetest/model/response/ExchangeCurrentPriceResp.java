package com.tpi.currentpricetest.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ExchangeCurrentPriceResp {

    private String updateTime;
    private Map<String,ExchangeCurrentPriceDetailResp> currentPriceDetail;

}
