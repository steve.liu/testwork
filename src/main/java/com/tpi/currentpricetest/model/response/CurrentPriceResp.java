package com.tpi.currentpricetest.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CurrentPriceResp {

    private CurrentPriceTimeResp time;
    private String disclaimer;
    private String chartName;
    private Map<String,CurrentPriceBpiDetailResp> bpi;

}
