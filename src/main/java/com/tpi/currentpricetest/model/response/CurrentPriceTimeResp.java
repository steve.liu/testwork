package com.tpi.currentpricetest.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CurrentPriceTimeResp {

    private String updated;
    private String updatedISO;
    private String updateduk;
}
