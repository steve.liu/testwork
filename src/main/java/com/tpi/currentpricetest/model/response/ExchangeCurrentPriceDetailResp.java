package com.tpi.currentpricetest.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class ExchangeCurrentPriceDetailResp {

    private String code;
    private String currencyZH;
    private String rate;

}
