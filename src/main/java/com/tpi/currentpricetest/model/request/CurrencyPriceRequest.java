package com.tpi.currentpricetest.model.request;


import lombok.Builder;
import lombok.Data;


@Builder
@Data
public class CurrencyPriceRequest {

        /**
         * 幣別英文
         */
        private String enCurrency;

        /**
         * 幣別中文
         */
        private String zhCurrency;
}
