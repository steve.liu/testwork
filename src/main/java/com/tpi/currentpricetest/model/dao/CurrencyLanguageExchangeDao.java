package com.tpi.currentpricetest.model.dao;

import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.Id;


/**
 * 幣別中英文對照資訊
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currency_language_exchange")

public class CurrencyLanguageExchangeDao {

        /**
         * 幣別英文
         */
        @Id
        @Column(name = "en_currency")
        private String enCurrency;

        /**
         * 幣別中文
         */
        @Column(name = "zh_currency")
        private String zhCurrency;
}
