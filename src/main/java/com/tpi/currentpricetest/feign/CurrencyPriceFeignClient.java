package com.tpi.currentpricetest.feign;

import com.tpi.currentpricetest.configuration.FeignConfiguration;
import com.tpi.currentpricetest.model.response.CurrentPriceResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "${coindesk.url}", name = "currentPrice", configuration = FeignConfiguration.class)
public interface CurrencyPriceFeignClient {

    @GetMapping("/v1/bpi/currentprice.json")
    ResponseEntity<CurrentPriceResp> callCoinDesk();
}
