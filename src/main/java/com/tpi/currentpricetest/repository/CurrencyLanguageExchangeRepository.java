package com.tpi.currentpricetest.repository;

import com.tpi.currentpricetest.model.dao.CurrencyLanguageExchangeDao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyLanguageExchangeRepository extends JpaRepository <CurrencyLanguageExchangeDao, Long>{
    public Optional<CurrencyLanguageExchangeDao> findByEnCurrency(String enCurrency);
    public Optional<CurrencyLanguageExchangeDao> findByZhCurrency(String zhCurrency);
}
