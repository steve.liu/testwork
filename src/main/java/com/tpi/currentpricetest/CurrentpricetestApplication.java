package com.tpi.currentpricetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrentpricetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrentpricetestApplication.class, args);
	}

}
