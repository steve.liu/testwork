package com.tpi.currentpricetest.controller;


import com.tpi.currentpricetest.model.response.CurrentPriceResp;
import com.tpi.currentpricetest.model.response.ExchangeCurrentPriceResp;
import com.tpi.currentpricetest.service.CurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class CurrencyApiController {

    @Autowired
    private CurrencyService currencyService;

    /**
     * 呼叫coinDesk的API
     * @return CurrentPriceResp api回傳json
     */
    @PostMapping(value = "/callCoinDesk")
    public CurrentPriceResp callCoinDesk() {
        return currencyService.callCoinDesk();
    }

    /**
     * 呼叫coinDesk並轉換內容的API
     * @return CurrentPriceResp api回傳json
     */
    @PostMapping(value = "/exchangeCoinDesk")
    public ExchangeCurrentPriceResp exchangeCoinDesk() {
        return currencyService.exchangeCoinDesk();
    }


}
