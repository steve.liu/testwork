package com.tpi.currentpricetest.controller;

import com.tpi.currentpricetest.model.dao.CurrencyLanguageExchangeDao;
import com.tpi.currentpricetest.model.request.CurrencyPriceRequest;
import com.tpi.currentpricetest.service.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DBApiController {

    @Autowired
    private  DBService dbService;

    /**
     * 新增資料庫資料
     * @param currencyLanguageExchangeDao 幣別資料
     * @return boolean
     */
    @PostMapping(value = "/insertData")
    public boolean insertData(CurrencyPriceRequest currencyPriceRequest) {
        return dbService.insertData(currencyPriceRequest.getEnCurrency() , currencyPriceRequest.getZhCurrency());
    }

    /**
     * 修改資料庫資料
     * @param currencyLanguageExchangeDao 幣別資料
     * @return boolean
     */
    @PostMapping(value = "/updateData")
    public boolean updateData(CurrencyPriceRequest currencyPriceRequest) {
        return dbService.updateData(currencyPriceRequest.getEnCurrency() , currencyPriceRequest.getZhCurrency());
    }

    /**
     * 刪除資料庫資料
     * @param currencyLanguageExchangeDao 幣別資料
     * @return boolean
     */
    @PostMapping(value = "/deleteData")
    public boolean deleteData(CurrencyPriceRequest currencyPriceRequest) {
        return dbService.deleteData(currencyPriceRequest.getEnCurrency() , currencyPriceRequest.getZhCurrency());
    }

    /**
     * 查詢資料庫資料
     * @param currencyLanguageExchangeDao 幣別資料
     * @return CurrencyLanguageExchangeDao 查詢資料結果
     */
    @PostMapping(value = "/selectData")
    public CurrencyLanguageExchangeDao selectData(CurrencyPriceRequest currencyPriceRequest) {
        return dbService.selectData(currencyPriceRequest.getEnCurrency() , currencyPriceRequest.getZhCurrency());
    }


}
