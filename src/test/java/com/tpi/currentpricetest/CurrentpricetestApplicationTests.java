package com.tpi.currentpricetest;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tpi.currentpricetest.controller.DBApiController;
import com.tpi.currentpricetest.model.request.CurrencyPriceRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@Slf4j
@AutoConfigureMockMvc
class CurrentpricetestApplicationTests extends BaseTest{


	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void callCoinDesk() throws Exception{
		String url =
				new UrlBuilder<DBApiController>()
						.setControllerClass(DBApiController.class)
						.setHttpMethod(HttpMethod.POST)
						.setMethodName("callCoinDesk")
						.build();
		this.mockMvc.perform(
					MockMvcRequestBuilders.post(url))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(print())
					.andReturn();
	}

	@Test
	void exchangeCoinDesk() throws Exception{
		String url =
				new UrlBuilder<DBApiController>()
						.setControllerClass(DBApiController.class)
						.setHttpMethod(HttpMethod.POST)
						.setMethodName("exchangeCoinDesk")
						.build();

		this.mockMvc.perform(
					MockMvcRequestBuilders.post(url))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(print())
					.andReturn();
	}

	@Test
	void insertData() throws Exception{
		String url =
				new UrlBuilder<DBApiController>()
						.setControllerClass(DBApiController.class)
						.setHttpMethod(HttpMethod.POST)
						.setMethodName("insertData", CurrencyPriceRequest.class)
						.build();
		CurrencyPriceRequest content = CurrencyPriceRequest.builder()
														   .enCurrency("TWD")
														   .zhCurrency("新台幣")
														   .build();
		this.mockMvc.perform(
					MockMvcRequestBuilders.post(url)
										  .contentType(MediaType.APPLICATION_JSON)
										  .content(objectMapper.writeValueAsString(content)))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(print())
					.andReturn();
	}

	@Test
	void deleteData() throws Exception{
		String url =
				new UrlBuilder<DBApiController>()
						.setControllerClass(DBApiController.class)
						.setHttpMethod(HttpMethod.POST)
						.setMethodName("deleteData", CurrencyPriceRequest.class)
						.build();
		CurrencyPriceRequest content = CurrencyPriceRequest.builder()
														   .enCurrency("USD")
														   .zhCurrency(null)
														   .build();
		this.mockMvc.perform(
					MockMvcRequestBuilders.post(url)
										  .contentType(MediaType.APPLICATION_JSON)
										  .content(objectMapper.writeValueAsString(content)))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(print())
					.andReturn();
	}

	@Test
	void selectData() throws Exception{
		String url =
				new UrlBuilder<DBApiController>()
						.setControllerClass(DBApiController.class)
						.setHttpMethod(HttpMethod.POST)
						.setMethodName("insertData", CurrencyPriceRequest.class)
						.build();
		CurrencyPriceRequest content = CurrencyPriceRequest.builder()
														   .enCurrency("USD")
														   .zhCurrency(null)
														   .build();
		this.mockMvc.perform(
					MockMvcRequestBuilders.post(url)
										  .contentType(MediaType.APPLICATION_JSON)
										  .content(objectMapper.writeValueAsString(content)))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(print())
					.andReturn();
	}

	@Test
	void updateData() throws Exception{
		String url =
				new UrlBuilder<DBApiController>()
						.setControllerClass(DBApiController.class)
						.setHttpMethod(HttpMethod.POST)
						.setMethodName("insertData", CurrencyPriceRequest.class)
						.build();
		CurrencyPriceRequest content = CurrencyPriceRequest.builder()
														   .enCurrency("USD")
														   .zhCurrency("美國的錢")
														   .build();
		this.mockMvc.perform(
					MockMvcRequestBuilders.post(url)
										  .contentType(MediaType.APPLICATION_JSON)
										  .content(objectMapper.writeValueAsString(content)))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(print())
					.andReturn();
	}

}
