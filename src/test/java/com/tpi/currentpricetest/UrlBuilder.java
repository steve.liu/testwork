package com.tpi.currentpricetest;

import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpMethod.*;

public class UrlBuilder<T> {
    private Class<T> controllerClass;
    private String methodName;
    private HttpMethod httpMethod;
    private Class<?>[] parameterTypes;

    public UrlBuilder() {
    }

    public UrlBuilder<T> setMethodName(String methodName, Class<?>... parameterTypes) {
        this.methodName = methodName;
        this.parameterTypes = parameterTypes;
        return this;
    }

    public UrlBuilder<T> setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
        return this;
    }

    public UrlBuilder<T> setControllerClass(Class<T> controllerClass) {
        this.controllerClass = controllerClass;
        return this;
    }

    public String build() throws NoSuchMethodException {
        String url = this.controllerClass.getAnnotation(RequestMapping.class).value()[0];
        String[] value = new String[0];
        if (GET.equals(this.httpMethod)) {
            value = this.controllerClass.getDeclaredMethod(this.methodName, this.parameterTypes)
                                        .getAnnotation(GetMapping.class).value();
        } else if (POST.equals(this.httpMethod)) {
            value = this.controllerClass.getDeclaredMethod(this.methodName, this.parameterTypes)
                                        .getAnnotation(PostMapping.class).value();
        } else if (PUT.equals(this.httpMethod)) {
            value = this.controllerClass.getDeclaredMethod(this.methodName, this.parameterTypes)
                                        .getAnnotation(PutMapping.class).value();
        } else if (PATCH.equals(this.httpMethod)) {
            value = this.controllerClass.getDeclaredMethod(this.methodName, this.parameterTypes)
                                        .getAnnotation(PatchMapping.class).value();
        } else if (DELETE.equals(this.httpMethod)) {
            value = this.controllerClass.getDeclaredMethod(this.methodName, this.parameterTypes)
                                        .getAnnotation(DeleteMapping.class).value();
        }

        String subUrl = value.length == 0 ? "" : value[0];
        return url + subUrl;
    }
}
